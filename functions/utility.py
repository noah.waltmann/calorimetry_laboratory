from typing import Dict, List, Tuple

import h5py as h5
import matplotlib.pyplot as plt
import numpy as np


def plot_temp_over_time(
    data: List[np.ndarray],
    time: List[np.ndarray],
    legend: List[str],
    x_label: str,
    y_label: str,
) -> None:
    """Plots temperature data over time with error bars for multiple datasets.

    This function creates a plot representing temperature data against time
    for multiple sensors or datasets. Each dataset's standard deviation is visualized
    with error bars.

     Args:
        data (List[np.ndarray]): A list of numpy arrays where each array represents
                                 the temperature data (with standard deviation).
                                 Each array should have a shape of (2, n), with n
                                 representing the number of data points.
        time (List[np.ndarray]): A list of numpy arrays with the time data corresponding
                                 to each dataset in `data`.
        legend (List[str]): A list of strings that label each dataset in the legend.
        x_label (str): The label for the x-axis (time).
        y_label (str): The label for the y-axis (temperature).

    """
    # init the matplotlib.axes.Axes and matplotlib.figure.Figure Object for later plot
    fig, ax = plt.subplots(1, 1)
    markers = ["o", "^", "2", "p", "D"]

    for i in range(len(data)):
        # TODO: draw a plot using the ax.errorbar(...) function
        # Document: https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.errorbar.html

        ax.errorbar(x=list(time[i]),y=list(data[i][0,:]),yerr=list(data[i][1,:]),capsize=1,linewidth=1)

        # DONE #

    # Errorbars removed from Legend
    legend_handles, labels = ax.get_legend_handles_labels()
    legend_handles = [h[0] for h in legend_handles]

    # TODO: set legend, x- and y- axis label.

    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.legend(legend)
   
    # DONE #

    ax.ticklabel_format(scilimits=(0, 3))


def get_plot_data_from_dataset(
    data_path: str, group_path: str
) -> Dict[str, np.ndarray]:
    """Get the necessary data from the dataset to plot.

    This function returns the data in a HDF5 file in all subgroups of a group in 'group_path'
    and automatically categorizes and names the data based on the name of the dataset as well as the metadata.

    Args:
        data_path (str): path to HDF5 file.
        group_path (str): path in HDF5 to group.

    Returns:
        Dict[str, np.ndarray]: Data for plot in a dict.

    Example:
        Output (example data):
        {
            "temperature": np.array([
                            [24.89, 24.92, 24.00, 25.39],
                            [24.89, 24.92, 24.00, 25.39],
                            [24.89, 24.92, 24.00, 25.39]
                        ]) -> temperature from each sensor, The first dimension(row) represents the sensor.
            "timestamp": np.array([
                            [0.43, 1.60, 3.05, 4.25],
                            [0.81, 2.13, 3.49, 4.62],
                            [1.34, 2.60, 3.85, 5.08],
                        ]) -> timestamp for each sensor, The first dimension(row) represents the sensor.
            "name": np.array(["sensor_1", "sensor_2", "sensor_3"]) -> name of each sensor should be hier
        }

    """
    temperature = []
    time = []
    name = []
    with h5.File(data_path) as data:
        group = data[group_path]
        subgroups = []
        min_len = None
        start_time = None

        for subgroup in group:
            subgroups.append(subgroup)
            try:
                dataset_start_time = group[subgroup]["timestamps"][0]
                dataset_len = len(group[subgroup]["timestamps"])

                # Find the minimum length of the data set.
                if min_len is None:
                    min_len = dataset_len
                elif dataset_len < min_len:
                    min_len = dataset_len

                # Only group with dataset called timestamp will be read.
            except KeyError:
                continue

            # TODO: Find the start time point of the measurement.
            # smallest start-time is saved in start_time
            if start_time is None:
                start_time = dataset_start_time
            elif dataset_start_time < start_time:
                start_time = dataset_start_time

            # DONE #

        for subgroup in subgroups:
            # TODO: Save data in to the lists temperature, time and name.
            # Data for each sensor must have the same length because of np.ndarray will be use in the output.
            
            #temperatures, timestamps, and names are saved in lists 
            try:
                temperature.append(list(group[subgroup]["temperatures"][:min_len]))
                time.append(list(group[subgroup]["timestamps"][:min_len]))
                name.append(group[subgroup].attrs["name"])
            except KeyError:
                continue
            # DONE #

    # TODO: return the output dict.
    
    # lists are converted to np.arrays
    return({"temperature":np.array(temperature),"time":np.array(time),"name":np.array(name)})

    # DONE #


def cal_mean_and_standard_deviation(data: np.ndarray) -> np.ndarray:
    """Calculating mean and standard deviation for raw data of multiple sensors.

    Args:
        data (np.ndarray): raw data in a 2 dimensional array (m, n), the first dimension should not be 1 if
                           there are multiple measurements at the same time (and place).

    Returns:
        np.ndarray: mean of raw data with standard deviation in a 2D ndarray with shape (2, n).

    """
    # TODO: Calculate the mean and standard deviation of the first dimension and return the result as a
    # two-dimensional (2, n)-shaped ndarray.
    alltemps=[]
    output=np.empty(shape=(2,len(data[0])))
    for j in range(len(data[0])):
        temps=[]
        for i in range(len(data)):
            temps.append(data[i-1][j-1])
            
        output[0,j-1]=np.mean(temps)
        output[1,j-1]=np.std(temps)
    return(output)
    # DONE #


def get_start_end_temperature(
    temperature_data: np.ndarray, threshold: float = 0.05
) -> Tuple[float, float]:
    """Calculates the high and low temperatures from a dataset.

    This function computes the (average of) the highest temperatures and the (average of) the lowest temperatures
    within a given threshold from the maximum and minimum temperatures recorded in the dataset. These are
    considered as the ending and starting temperatures respectively.

    Args:
        temperature_data (np.ndarray): The temperature dataset as a 2D numpy array.
        threshold (float): The threshold percentage used to identify temperatures close to the maximum
                           and minimum values as high and low temperatures respectively. Default to 0.05

    Returns:
        Tuple[float, float]: A tuple containing the average high temperature first and the average low
                             temperature second.

    """
    # TODO: You don't have to implement this function exactly as docstring expresses it, it just gives
    # an idea that you can refer to. The goal of this function is to obtain from the data the high and
    # low temperatures necessary to calculate the heat capacity.
    max_avg_temp=max(temperature_data[0])
    min_avg_temp=min(temperature_data[0])
    high_avg_temps=[]
    low_avg_temps=[]
    for i in range(len(temperature_data)):
        if temperature_data[0][i]>max_avg_temp-threshold:
            high_avg_temps.append(temperature_data[0][i])
        elif temperature_data[0][i]<min_avg_temp+threshold:
            low_avg_temps.append(temperature_data[0][i])
    
    high_avg_temps.append(max_avg_temp)
    low_avg_temps.append(min_avg_temp)
    
    avg_high_temp=sum(high_avg_temps)/len(high_avg_temps)
    avg_low_temp=sum(low_avg_temps)/len(low_avg_temps)
    return(avg_high_temp,avg_low_temp)

    # DONE #
